require 'rails_helper'

RSpec.describe Book, type: :model do
    describe "associations" do
        it { should belong_to(:author) }
    end

    describe "validations" do
        it { should validate_presence_of(:title) }
        it { should_not allow_value("", nil).for(:title) }
    end

    describe "associations" do
        it { should belong_to(:author) }
    end

    describe "validations" do
        it { should validate_presence_of(:title) }
        it { should_not allow_value("", nil).for(:title) }
    end

    describe "associations" do
        it { should belong_to(:author) }
    end

    describe "validations" do
        it { should validate_presence_of(:title) }
        it { should_not allow_value("", nil).for(:title) }
    end

    describe "associations" do
        it { should belong_to(:author) }
    end
end