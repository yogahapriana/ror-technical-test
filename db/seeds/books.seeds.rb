after :authors do
    authors = Author.all
    authors.each do |author|
        book = Book.new
        book.title = Faker::Book.title
        book.author = author
        book.published_year = (Date.today-rand(10000)).year
        book.save
    end
end