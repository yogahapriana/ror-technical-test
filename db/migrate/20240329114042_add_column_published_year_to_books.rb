class AddColumnPublishedYearToBooks < ActiveRecord::Migration[7.1]
  def change
    add_column :books, :published_year, :integer, null: false
  end
end
