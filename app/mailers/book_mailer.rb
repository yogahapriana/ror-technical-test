class BookMailer < ApplicationMailer
    def book_creation
        @book = params[:book]
        @user = params[:user]

        mail(to: @user.email, subject: 'Book Creation Notification')
    end
end
