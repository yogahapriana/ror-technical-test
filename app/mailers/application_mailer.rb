class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@ror-technical-test.com"
  layout "mailer"
end
