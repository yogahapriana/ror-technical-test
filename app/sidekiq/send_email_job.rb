class SendEmailJob
  include Sidekiq::Job

  def perform(book_id, user_id)
    book = Book.find_by_id(book_id)
    user = User.find_by_id(user_id)

    BookMailer.with(book: book, user: user).book_creation.deliver_now
  end
end
