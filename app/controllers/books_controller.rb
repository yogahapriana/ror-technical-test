class BooksController < ApplicationController
    before_action :authenticate_user!
    before_action :find_book, only: :show

    def index
        @books = Book.paginate(page: params[:page], per_page: 10)
    end

    def show
    end

    def new
        @book = Book.new
        @authors = Author.all.map { |author| [author.name, author.id] }
    end

    def create
        @book = Book.create(book_params.merge(created_by: current_user.id))

        if @book.save
            SendEmailJob.perform_async(@book.id, current_user.id)
            redirect_to books_path, notice: "Book has been created"
        else
            render :new
        end
    end

    private

    def find_book
        @book = Book.find_by_id(params[:id])

        if @book.blank?
            redirect_to books_path, alert: "Books not found"
        end
    end

    def book_params
        params.require(:book).permit(:title, :author_id, :published_year)
    end
end
