class Api::V1::BooksController < ApplicationController
    def index
        @books = Book.paginate(page: params[:page], per_page: params[:per_page].blank? ? 10 : params[:per_page])
        render json: {
            data: @books,
            status: :success,
            code: 200,
            message: "Book data retrieved successfully",
            metadata: {
                page: params[:page].blank? ? 1 : params[:page].to_i,
                count: @books.total_entries,
                total_page: @books.total_pages
            }
        }
    end
end
