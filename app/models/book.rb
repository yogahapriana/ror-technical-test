class Book < ApplicationRecord
  belongs_to :author

  validates :title, presence: true, allow_blank: false
end
