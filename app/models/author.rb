class Author < ApplicationRecord
    validates :name, presence: true, allow_blank: false

    has_many :books
end
